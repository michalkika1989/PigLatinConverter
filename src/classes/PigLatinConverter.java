package classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author kikaamic
 *
 */

public class PigLatinConverter {

	private final String VOWELS = "aeiou";
	private final String CONSONANTS = "bcdfghjklmnpqrstvxwyz";
	private final String SPLITTER = " ";
	private final String ENDING_AY = "ay";
	private final String ENDING_WAY = "way";
	private final String DOT = ".";
	private final String APOSTROPHE = "�";
	private final String SPACE = " ";
	private final String HYPHEN = "-";	
	private final String EMPTY = "";
	private final String COMMA = ",";
	
	private List<String> words;

	public PigLatinConverter() {
		this.words = new ArrayList<>();
	}
	
	/**
	 * Set sentence and call start
	 * @param sentence
	 * @return PigLatinConverter
	 */
	public PigLatinConverter setSentence(final String sentence){
		this.words = Arrays.asList(sentence.split(SPLITTER));
		return this;
	}
	
	/**
	 * Starts processing sentence
	 * @return non-nullable pig-latin sentence
	 */
	public String start(){
		return process(words);
		
	}	
	
	/**
	 * Process of replacing
	 * @param words
	 * @return non-nullable sentence
	 */
	private String process(final List<String> words){
		StringBuilder result = new StringBuilder();
		for(String word : words){			
			if(word != null && word.length() > 0){
				if(isContainingHyphen(word)){
					List<String> hyphenWords = Arrays.asList(word.split(HYPHEN));
					String hyphenResult = process(hyphenWords).replace(SPACE, HYPHEN);
					result.append(hyphenResult);
				}else if(isEndingWithWay(word)){
					result.append(word);
				}else if(isFirstLetterConsonant(word)){
					result.append(capitalizeResultByPattern(moveFirstToEndAddAY(word), word));
				}else if(isFirstLetterVowel(word)){
					result.append(capitalizeResultByPattern(toEndAddWAY(word), word));
				}else{ //is something else as number
					result.append(word);
				}
			}
			
			if(!word.equals(words.get(words.size()-1))){
				result.append(SPACE);
			}
		}
		
		return result.toString();
	}

	//Block of checking strings
	/**
	 * Check if first letter is consonant
	 * @param word
	 * @return true if first letter is consonant, false otherwise
	 */
	private boolean isFirstLetterConsonant(final String word){
		return CONSONANTS.contains(word.substring(0, 1).toLowerCase());
	}
	
	/**
	 * Check if first letter is vowel
	 * @param word
	 * @return true if first letter is vowel, false otherwise
	 */
	private boolean isFirstLetterVowel(final String word){		
		return VOWELS.contains(word.substring(0, 1).toLowerCase());
	}
	
	/**
	 * Check if word ends with WAY 
	 * @param word
	 * @return true if ends with WAY, false otherwise
	 */
	private boolean isEndingWithWay(final String word){				
		return word.toLowerCase().endsWith(ENDING_WAY);
	}	
	
	/**
	 * Check if word contains hyphen
	 * @param word
	 * @return true if contain hyphen, false otherwise
	 */
	private boolean isContainingHyphen(final String word){
		return word.contains(HYPHEN);
	}	
	
	//Block of moving and appending letters
	/**
	 * Method is moving first letter to the end and append AY 
	 * @param word
	 * @return non-nullable string
	 */
	private String moveFirstToEndAddAY(final String word){
		StringBuilder result = new StringBuilder();			
		result.append(word.substring(1));
		result.append(word.substring(0, 1));
		result.append(ENDING_AY);			
				
		return wordMarksToRightPosition(result.toString().toLowerCase());
	}	
	
	/**
	 * Method append WAY to the end of the string
	 * @param word
	 * @return non-nullable string
	 */
	private String toEndAddWAY(final String word){
		StringBuilder result = new StringBuilder();
		result.append(word);
		result.append(ENDING_WAY);			
		
		return wordMarksToRightPosition(result.toString().toLowerCase());
	}	
	
	//Block of replacing dots, apostrophes, commas
	/**
	 * Method is setting word marks to right positions
	 * @param word
	 * @return non-nullable string
	 */
	private String wordMarksToRightPosition(final String word){
		String result = "";
		if(word.contains(APOSTROPHE)){
			result = moveApostrophe(word);
		}else if(word.contains(DOT)){
			result = moveDot(word);
		}else if(word.contains(COMMA)){
			result = moveComma(word);
		}else {
			result = word;
		}
		
		return result;
	}
	
	/**
	 * Method find and move "." to the end of string
	 * @param word
	 * @return non-nullable string
	 */
	private String moveDot(final String word){
		StringBuilder result = new StringBuilder();
		result.append(word.replace(DOT, EMPTY));
		result.append(DOT);
		
		return result.toString();
	}
	
	/**
	 * Method find and move "�" in string before last character
	 * @param word
	 * @return non-nullable string
	 */
	private String moveApostrophe(final String word){
		StringBuilder result = new StringBuilder();
		String temp = word.replace(APOSTROPHE, EMPTY);
		result.append(temp.substring(0, temp.length()-1));
		result.append(APOSTROPHE);
		result.append(temp.substring(temp.length()-1));
				
		return result.toString();
	}
	
	/**
	 * Method move comma to end of the word
	 * @param word
	 * @return non-nullable string
	 */
	private String moveComma(final String word){
		StringBuilder result = new StringBuilder();
		result.append(word.replace(COMMA, EMPTY));
		result.append(COMMA);
				
		return result.toString();
	}		
	
	//Block of capitalization of strings
	/**
	 * Method capitalize letters in first string by pattern string
	 * @param capitalize
	 * @param pattern
	 * @return capitalized string, non-nullable
	 */
	private String capitalizeResultByPattern(final String capitalize, final String pattern){
		return capitalize(capitalize, capitalLetterIndexes(pattern));
	}
	
	/**
	 * Method check string and return all positions where letters are upperCase
	 * @param word
	 * @return list of positions
	 */
	private List<Integer> capitalLetterIndexes(final String word){
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < word.length(); i++){
			if(Character.isUpperCase(word.charAt(i))){
				result.add(i);
			}
		}
		return result;
	}
	
	/**
	 * Method capitalize chars in string on all defined indexes 
	 * @param word
	 * @param indexes
	 * @return non-nullable string
	 */
	private String capitalize(final String word, final List<Integer> indexes)
	{
	    StringBuilder result = new StringBuilder(word);
	    for(int index : indexes){
	    	result.setCharAt(index, Character.toUpperCase(result.charAt(index)));
	    }	    
	    return result.toString();
	}	
}






