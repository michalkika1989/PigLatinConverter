package classes;

/**
 * 
 * @author kikaamic
 *
 */

public class Main {
	
	/**
	 * Write some Java code that translates a string (word, sentence, or paragraph) into �pig-latin� using the following rules.

		Words that start with a consonant have their first letter moved to the end of the word and the letters �ay� added to the end.
		
			Hello becomes Ellohay
		
		Words that start with a vowel have the letters �way� added to the end.
		
			apple becomes appleway
		
		Words that end in �way� are not modified.
		
			stairway stays as stairway
		
		Punctuation must remain in the same relative place from the end of the word.
		
			can�t becomes antca�y		
			end. becomes endway.
		
		Hyphens are treated as two words
		
			this-thing becomes histay-hingtay
		
		Capitalization must remain in the same place.
		
			Beach becomes Eachbay		
			McCloud becomes CcLoudmay
			
	 */

	public static void main(String[] args) {

		PigLatinConverter converter = new PigLatinConverter();
		
		Long start = System.currentTimeMillis();
		System.out.println(converter.setSentence("Hello apple. stairway can�t this-thing Beach McCloud other�s").start());
		System.out.println("When we consider a Java program, it can be defined as a collection of objects that communicate via invoking each other's methods. Let us now briefly look into what do class, object, methods, and instance variables mean.");
		System.out.println(converter.setSentence("When we consider a Java program, it can be defined as a collection of objects that communicate via invoking each other�s methods. Let us now briefly look into what do class, object, methods, and instance variables mean.").start());
		System.out.println(System.currentTimeMillis() - start + "ms");

	}
}
